package com.example.quizapp

import android.util.Log
import org.hamcrest.CoreMatchers.instanceOf
import org.junit.Assert.*
import org.junit.Test

class ConstantsTest {

    @Test
    fun get_questions() {
        val questionList = Constants.getQuestions()
        assertThat(questionList, instanceOf( ArrayList<Question>().javaClass ) )
    }
}