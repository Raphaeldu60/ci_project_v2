package com.example.quizapp

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)


    @Test
    fun enterName() {
        onView(withId(R.id.et_name)).perform(typeText("Steve"))
        onView(withId(R.id.btn_start)).perform(click())
        onView(withId(R.id.iv_image)).check(matches(isDisplayed()))
    }
    @Test
    fun state1() {
        this.enterName()
        onView(withText("1/10")).check(matches(isDisplayed()))
    }
    @Test
    fun clickSubmitWithNoSelect() {
        this.enterName()
        onView(withId(R.id.btn_submit)).perform(scrollTo())
        onView(withId(R.id.btn_submit)).perform(click())
        onView(withId(R.id.btn_submit)).perform(click())
        onView(withText("1/10")).check(matches(isDisplayed()))
    }
    @Test
    fun clickSubmitWithSelect() {
        this.enterName()
        onView(withId(R.id.tv_option_one)).perform(click())
        onView(withId(R.id.btn_submit)).perform(scrollTo())
        onView(withId(R.id.btn_submit)).perform(click())
        onView(withId(R.id.btn_submit)).perform(click())
        onView(withText("2/10")).check(matches(isDisplayed()))
    }

    @Test
    fun clickSubmitToEnd(){
        this.enterName()
        for (i in 1..10) {
            onView(withId(R.id.tv_option_one)).perform(click())
            onView(withId(R.id.btn_submit)).perform(scrollTo())
            onView(withId(R.id.btn_submit)).perform(click())
            onView(withId(R.id.btn_submit)).perform(click())
        }
        onView(withText("Steve")).check(matches(isDisplayed()))
    }

    @Test
    fun clickFinishAfterEnd(){
        this.clickSubmitToEnd()
        onView(withId(R.id.btn_finish)).perform(click())

    }
}