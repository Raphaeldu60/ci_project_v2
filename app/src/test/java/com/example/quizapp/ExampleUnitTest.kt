package com.example.quizapp

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    private val quizQuestionsActivity: QuizQuestionsActivity = QuizQuestionsActivity()

    @Test
    fun addition_isCorrect() {
        quizQuestionsActivity.onClick(v = null)
        assertEquals(quizQuestionsActivity.mCurrentPosition, 1)
    }
}